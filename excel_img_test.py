##############################################################################
#
# An example of inserting images into a worksheet using the XlsxWriter
# Python module.
#
# Copyright 2013-2017, John McNamara, jmcnamara@cpan.org
#
import xlsxwriter
 
 
# 创建一个新Excel文件并添加一个工作表。
workbook = xlsxwriter.Workbook('images.xlsx')
worksheet = workbook.add_worksheet()
 
# 设置宽度
worksheet.set_column('A:A', 30)
# 设置高度
worksheet.set_row(0, 80)


worksheet.insert_image('A1', 'touxiang.png', {'x_scale': 0.3, 'y_scale': 0.3})

worksheet.write('B1', '向单元格插入一张图片：')
 
 
workbook.close()