import hashlib
import base64
import hmac
import json

import requests


import sys

PY3 = sys.version_info[0] == 3

if PY3:
    from urllib.parse import quote, urlencode

    def b(s):
        if isinstance(s, str):
            return s.encode('utf-8')
        return s

    builtin_str = str
    str = str
    bytes = bytes

    def stringify(data):
        return data

else:
    from urllib import quote, urlencode

    def b(s):
        return s

    builtin_str = str
    str = unicode  # noqa
    bytes = str

    def stringify(data):
        if isinstance(data, dict):
            return dict([(stringify(key), stringify(value))
                         for key, value in data.iteritems()])
        elif isinstance(data, list):
            return [stringify(element) for element in data]
        elif isinstance(data, unicode):  # noqa
            return data.encode('utf-8')
        else:
            return data


__all__ = [
    'quote', 'urlencode'
]

DEFAULT_CHUNKSIZE = 8192


def make_content_md5(value, chunksize=DEFAULT_CHUNKSIZE):
    if hasattr(value, 'fileno'):
        md5 = hashlib.md5()
        for chunk in iter(lambda: value.read(chunksize), b''):
            md5.update(chunk)
        value.seek(0)
        return md5.hexdigest()
    elif isinstance(value, bytes) or (not PY3 and
                                      isinstance(value, builtin_str)):
        return hashlib.md5(value).hexdigest()
    else:
        raise UpYunClientException('object type error')


def decode_msg(msg):
    if isinstance(msg, bytes):
        msg = msg.decode('utf-8')
    return msg


def encode_msg(msg):
    if isinstance(msg, str):
        msg = msg.encode('utf-8')
    return msg


def make_policy(data):
    policy = json.dumps(data)
    return base64.b64encode(b(policy))


def make_signature(**kwargs):
    """The standard signature algorithm. avaliable kwargs:
    username: operator
    password: password md5
    method: GET, POST ...
    uri: uri
    date: fmt rfc1123
    *policy: params md5
    *content_md5: content md5
    *auth_server: the remote address of authentication server
    """
    for k in kwargs:
        if isinstance(kwargs[k], bytes):
            kwargs[k] = kwargs[k].decode()

    if kwargs.get('auth_server'):
        auth_server = kwargs.pop('auth_server')
        resp = requests.post(auth_server, json=kwargs)
        return resp.text

    signarr = [kwargs['method'], kwargs['uri'], kwargs['date']]
    if kwargs.get('policy'):
        signarr.append(kwargs['policy'])
    if kwargs.get('content_md5'):
        signarr.append(kwargs['content_md5'])
    signstr = '&'.join(signarr)
    signature = base64.b64encode(
        hmac.new(b(kwargs['password']), b(signstr),
                 digestmod=hashlib.sha1).digest()
    ).decode()
    return 'UPYUN %s:%s' % (kwargs['username'], signature)

import locale, datetime

locale.setlocale(locale.LC_TIME, 'en_US')
mydate = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')

def make_purge_signature(service, username, password, uri, date):
    signstr = '&'.join([uri, service, date, password])
    signature = hashlib.md5(b(signstr)).hexdigest()
    return 'UpYun %s:%s:%s' % (service, username, signature)


data = {'username':'test123','password':'MBhFp7eH3x5CgFvgck0KPVrw2QIWu6O7','uri':'/touxiang.jpg','method':"POST",'date':mydate}

data_p = {"bucket":"123123123123","save-key":"/touxiang.jpg","expiration":1409200758}

print(make_signature(**data))
print(make_policy(str(data)))


# import upyun

# up = upyun.UpYun('123123123123', username='test123', password='MBhFp7eH3x5CgFvgck0KPVrw2QIWu6O7')

# #up.put('/ascii.txt', 'abcdefghijklmnopqrstuvwxyz\n')

# headers = { 'x-gmkerl-rotate': '180' }

# with open('./touxiang.png', 'rb') as f:
#     res = up.put('/touxiang.png', f, checksum=True, headers=headers)
