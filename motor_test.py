host = '127.0.0.1'
port = 27017
database = 'LiePin'

import time

start = time.clock()

import asyncio
from motor.motor_asyncio import AsyncIOMotorClient

connection = AsyncIOMotorClient(
    host,
    port
)
db = connection[database]

async def run():
    async for doc in db.LiePin_Analysis1.find({}, ['_id', 'JobTitle', 'is_end']):
        db.LiePin_Analysis1.update_one({'_id': doc.get('_id')}, {'$set': {'is_end':0}})

asyncio.get_event_loop().run_until_complete(run())

elapsed = (time.clock() - start)
print("Time used:",elapsed)