import time
import hmac
import hashlib
import base64
import urllib.parse

timestamp = str(round(time.time() * 1000))
secret = 'SEC1c2c8b49cc479c0545821db20896a883d5e99e2f48ad9e4e5122794ee3dc83cc'
secret_enc = secret.encode('utf-8')
string_to_sign = '{}\n{}'.format(timestamp, secret)
string_to_sign_enc = string_to_sign.encode('utf-8')
hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
sign = urllib.parse.quote(base64.b64encode(hmac_code))
# print(timestamp)
# print(sign)


import requests,json   #导入依赖库
headers={'Content-Type': 'application/json'}   #定义数据类型
webhook = 'https://oapi.dingtalk.com/robot/send?access_token=f5f0d4c81205a8281705151fe3520df81ae5c8725587da0350ebb1feb13c7a6a&timestamp='+timestamp+"&sign="+sign
#定义要发送的数据
#"at": {"atMobiles": "['"+ mobile + "']"
data = {
    "msgtype": "text",
    "text": {"content": '尊敬的王俊辙，您的审批被拒绝'},
    "isAtAll": True}
res = requests.post(webhook, data=json.dumps(data), headers=headers)   #发送post请求

print(res.text)