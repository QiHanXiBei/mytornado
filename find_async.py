import os

allname=[]

def getallfile(path):
    allfilelist=os.listdir(path)
    # 遍历该文件夹下的所有目录或者文件
    for file in allfilelist:
        filepath=os.path.join(path,file)
        # 如果是文件夹，递归调用函数
        if os.path.isdir(filepath):
            getallfile(filepath)
        # 如果不是文件夹，保存文件路径及文件名
        elif os.path.isfile(filepath) and filepath.endswith(".py") == True:
            text = ''
            with open(filepath,encoding='utf-8') as f:
                text = f.read()
            if text.find("async") != -1 and text.find("async_my") == -1 and text.find("apply_async") == -1 and text.find("asyncore") == -1:
                with open(filepath,"w",encoding='utf-8') as f:
                    f.write(text.replace("async","async_my"))
                    allname.append(file)
    return allname

print(getallfile('C:/Python37/lib/site-packages/pika'))