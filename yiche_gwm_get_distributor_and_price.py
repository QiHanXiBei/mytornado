#!/usr/bin/python
#coding:utf-8

import time,xlsxwriter,os,pymysql
import time,xlsxwriter,os,requests,json
import urllib,urllib.request
from urllib.parse import urlencode
from pyquery import PyQuery as pq


class YICHE_GWM:
    def __init__(self,url_distributor):
        self.url_distributor = url_distributor
        self.conn = pymysql.Connect(
            host='localhost',
            port=3306,
            user='root',
            password='123456',
            db='ocr_collect',
            charset='utf8',
            # cursorclass=pymysql.cursors.DictCursor  # 以字典的形式返回数据
        )
        # 获取游标
        self.cursor = self.conn.cursor()
        time.sleep(0.5)

    # excel文件日期补充
    def get_now(self):
        now = time.strftime("%Y-%m-%d_%H_%M_%S", time.localtime())
        return now

    # 获得今日日期
    def get_today(self):
        today = time.strftime("%Y-%m-%d", time.localtime())
        return today


    # 获得每个省份和车型link列表
    def by_url_get_pro_link(self):
        lis_pro_name_link = []
        model_list = ['fengjun5','fengjun7','pxilie']
        for m in range(len(model_list)):
            test_url = self.url_distributor+'beijing/'+model_list[m]+'/?BizModes=0'
            page = urllib.request.urlopen(url=test_url).read()
            page = pq(page)
            lis_pro_href = [i.attr('href') for i in page("#d_pro a").items()]
            lis_pro_name = [a.text() for a in page("#d_pro a").items()]
            for i in range(len(lis_pro_href)):
                if m ==0:
                    model_name = '风骏5'
                if m ==1:
                    model_name = '风骏7'
                if m ==2:
                    model_name = '炮'
                pro_info = (lis_pro_name[i],model_name,lis_pro_href[i])
                #('江西', '风骏5', '/jiangxi/fengjun5/?BizModes=0')
                lis_pro_name_link.append(pro_info)
        return lis_pro_name_link

    # 获得经销商店信息列表
    def by_pro_link_get_distributors(self,lis_pro_link):
        dict_list = []
        for p_m_info in lis_pro_link:
            page = urllib.request.urlopen(url=self.url_distributor+p_m_info[2]).read()
            page= pq(page)
            distributors_info = page("body > div.container.dealer-section > div.col-xs-9 > div.dealer-section-main > div.dealer-list-box.screen-box > div.main-inner-section.sm.dealer-box .dealer-list")
            page = pq(page)
            distributors_info = page(
                "body > div.container.dealer-section > div.col-xs-9 > div.dealer-section-main > div.dealer-list-box.screen-box > div.main-inner-section.sm.dealer-box .dealer-list")
            # 获得经销商名称
            name_list = distributors_info(" div.col-xs-6.left > h6 a")
            name_list = [na.text() for na in name_list.items()]
            try:
                name_list = [i.replace('4S店-','') for i in name_list]
            except:
                pass
            # 获得经销商林肯link
            shop_link_list = distributors_info(" div.col-xs-6.left > h6 a")
            shop_link_list = [hr.attr('href').split('?')[0] for hr in shop_link_list.items()]
            # 获得经销商地址
            address_info_list = page(
                "body > div.container.dealer-section > div.col-xs-9 > div.dealer-section-main > div.dealer-list-box.screen-box > div.main-inner-section.sm.dealer-box .add > span:nth-child(2)")
            try:
                address_info_list = [add.attr('title') for add in address_info_list.items()]
            except:
                address_info_list = 'null'
            # 获得市区
            city_list= page(
                "body > div.container.dealer-section > div.col-xs-9 > div.dealer-section-main > div.dealer-list-box.screen-box > div.main-inner-section.sm.dealer-box div:nth-child(2) > div > div.col-xs-7.middle > p")
            city_list = [c_d.text().split(' ')[0].replace('市','') for c_d in city_list.items()]
            # 进行数据解析，组成字典并将结果存入列表
            for n in range(len(name_list)):
                dict_info = {}
                try:
                    name = name_list[n]
                    if name.strip()== '':
                        name = 'null'
                except:
                    name = 'null'
                try:
                    address = address_info_list[n]
                    if address.strip() == '':
                        address = 'null'
                except:
                    address = 'null'
                try:
                    city = city_list[n]
                    if city.strip() == '':
                        city = 'null'
                except:
                    city = 'null'
                province = p_m_info[0]
                shop_link = shop_link_list[n]
                model = p_m_info[1]
                dict_info.update(brand = '皮卡',model = model,province = province,city = city,dealer = name,shop_link = shop_link,)
                print(dict_info)
                dict_list.append(dict_info)
        return dict_list

    # 获得车款
    def by_url_get_style(self,dict_list):
        # 5,7,炮
        n = 0
        error_list = []
        create_time = self.get_today()
        lis_model_code = ['cars_3263','cars_5484','cars_5722']
        for dict_info in dict_list:
            if dict_info['model'] == '风骏5':
                model_code = 'cars_3263'
            if dict_info['model'] == '风骏7':
                model_code = 'cars_5484'
            if dict_info['model'] == '炮':
                model_code = 'cars_5722'
            style_link = 'https:'+dict_info['shop_link']+model_code+'.html'
            print(style_link)
            page = urllib.request.urlopen(url=style_link).read()
            data = dict_info
            del data['shop_link']
            # 更换城市短名
            province = data['province']
            city = data['city']
            table_city = 'data_carwler_city'
            try:
                sql_ciyt = 'SELECT * FROM {table_city} WHERE province="{province}" AND  name="{city}"'.format(
                    table_city=table_city, province=province, city=city)
                self.cursor.execute(sql_ciyt)
                short_city_name = self.cursor.fetchone()[1]
                data['city'] = short_city_name
            except:
                pass
            page = pq(page)
            style_infos = page("body > div.worp > div.lfcont > div > div.car_list > div > div.car_price > table> tbody ")
            # 获得车款列表
            style_name = style_infos("td a")
            lis_style_name = [ sty.attr('title') for sty in style_name.items() if sty.attr('title')!= None]
            # 获取指导价格列表
            lis_guide_price = style_infos("td:nth-child(2) > text")
            lis_guide_price = [ g_p.text() for g_p in lis_guide_price.items()]
            # 获取降价额度
            lis_discount = style_infos("td:nth-child(3) > em > text")
            lis_discount = [dis.text().replace('↓','').strip() for dis in lis_discount.items()]
            # 获取现行价格
            lis_present_price = style_infos("td:nth-child(4) > a > text")
            lis_present_price = [pp.text() for pp in lis_present_price.items()]
            for j in range(len(lis_present_price)):
                try:
                    style = lis_style_name[j]
                    if style.strip() == '':
                        style = 'null'
                except:
                    style = 'null'
                try:
                    guide_price = lis_guide_price[j].replace('万','')
                    if guide_price.strip() == '':
                        guide_price = 'null'
                except:
                    guide_price = 'null'
                try:
                    present_price = lis_present_price[j].replace('万','')
                    if present_price.strip() == '':
                        present_price = 'null'
                except:
                    present_price = 'null'
                try:
                    discount = lis_discount[j].replace('万','')
                    if discount.strip() == '':
                        discount = '0.00'
                    if discount == '--':
                        discount = '0.00'
                except:
                    discount = '0.00'
                #print(guide_price,'----',discount,'----',present_price)
                data.update(style=style, guide_price=guide_price, present_price=present_price, discount=discount,
                            source='易车', create_time=create_time)
                print(data)
                table = 'data_carwler_price_monitor'
                brand = data['brand']
                dealer = data['dealer']
                model = data['model']
                style = data['style']
                source = data['source']
                keys = ','.join(data.keys())
                values = ','.join(['%s'] * len(data))
                # 删除旧有数据
                if n == 0:
                    try:
                        sql_del = 'DELETE FROM {table} WHERE brand="{brand}" AND  source="{source}"'.format(
                            table=table, brand=brand, source=source)
                        self.cursor.execute(sql_del)
                    except:
                        pass
                    n += 1
                # 插入数据
                try:
                    sql = 'INSERT INTO {table}({keys}) VALUES({values})'.format(table=table, keys=keys, values=values)
                    self.cursor.execute(sql, tuple(data.values()))
                    print('-----------------Successful',n)
                    self.conn.commit()
                except Exception as e:
                    error_list.append(dict_info)
                    self.conn.rollback()
        self.cursor.close()
        self.conn.close()
        print(error_list, '-------_-!-----------------------------error_list')
        return error_list

def the_main_fuc():
    url_distributor = 'https://dealer.yiche.com/'
    f = YICHE_GWM(url_distributor)
    lis_pro_link = f.by_url_get_pro_link()
    dict_list = f.by_pro_link_get_distributors(lis_pro_link)
    f.by_url_get_style(dict_list)
    return True

the_main_fuc()