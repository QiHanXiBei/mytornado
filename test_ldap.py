from ldap3 import Server, Connection, ALL,MODIFY_REPLACE

s = Server('localhost', get_info=ALL)  

c = Connection(s, user='cn=admin,dc=v3u,dc=cn', password='admin')
c.bind()

# print(c.extend.standard.who_am_i())

#exit(-1)


# c.add('cn=user1,ou=users,o=company', ['inetOrgPerson', 'posixGroup', 'top'], {'sn': 'user_sn', 'gidNumber': 0})
# print(c.result)

# c.delete('cn=user1,ou=users,o=company')
# print(c.result)

#添加组

# groupDN = 'cn=ADM_Local,ou=Staff,ou=RU,dc=v3u,dc=cn'
# objectClass = 'groupOfNames'
# attr = {
#   'cn': 'ADM_Local',
#   'member': 'uid=admin,ou=people,dc=domain,dc=local',
#   'description': 'local group for access to IPA'
# }

# c.add(groupDN , objectClass , attr)
# print(c.result)

# c.add('cn={},cn={},dc={},dc={}'.format('test','test','v3u','cn'), {'givenName': '测试账号','userPassword':'1234'})
# print(c.result)

# c.add('cn=test,cn=Users,dc=v3u,dc=cn', 'ADM_Local', {'sAMAccountName': 'test', 'userPrincipalName': 'test','givenName': 'test', 'sn': 'test', 'displayName': '测试用户', 'mail':'123@example.com'})
# print(c.result)

# print(c.search("dc=v3u,dc=cn", '(&(cn=testuser1))', attributes=['*']))
# print(c.entries)

# #添加组织
# res = c.add('OU=v3u_users,dc=v3u,dc=cn', object_class='OrganizationalUnit')
# print(res)
# print(c.result)

# 添加群组
# ldap_attr = {}
# ldap_attr['objectClass'] = ['top', 'posixGroup']
# ldap_attr['gidNumber'] = '1'

# c.add('cn=mygroup,dc=v3u,dc=cn',attributes=ldap_attr)
# print(c.result)

#添加用户
# c.add('CN=user1,OU=v3u_users,dc=v3u,dc=cn',object_class=['user'],attributes={'userPassword':'1234','sn':'测试'})
# print(c.result)

#添加用户
ldap_attr = {}
ldap_attr['cn'] = "test user1"
ldap_attr['sn'] = "测试"
ldap_attr['userPassword'] = "1234"

user_dn = "cn=testuser2,cn=mygroup,ou=v3u_users,dc=v3u,dc=cn"

c.add(dn=user_dn,object_class='inetOrgPerson',attributes=ldap_attr)
print(c.result)


# #删除用户
# c.delete(dn='cn=testuser1,cn=mygroup,dc=v3u,dc=cn')
# print(c.result)


# #修改用户
# c.modify('cn=testuser1,cn=mygroup,dc=v3u,dc=cn',{'uid':[(MODIFY_REPLACE, ['1'])]})
# print(c.result)