# import re

# mystr = '债券资产、银行存款30％――100％其他资产不高于70％银行有权根据市场情况，在不对客户实质性权益产生重大影响且根据约定提前公告的情况下，对本理财产品的投资范围、投资品种和投资比例进行调整'

# mylist = re.findall('(.+?)、(.+?)30\％――100\％(.+?)不高于70\％',mystr)

# print(mylist[0][0])
# print(mylist[0][1])
# print(mylist[0][2])

# mydict = eval("{'%s':'30%%――100%%','%s':'30%%――100%%','%s':'不高于70%%'}" % (mylist[0][0],mylist[0][1],mylist[0][2]))

# print(mydict)

# import redis

# r = redis.Redis(host='localhost')

# r.zadd('myrank', {'C': 100})
#     # redis.zadd('language', 'C', 100)
# r.zadd('myrank', {'Java': 30})
# r.zadd('myrank', {'Python': 85})

# print(r.zrange('myrank', 0, 10, desc=True,withscores=True))

# #修改分数
# newScore = r.zincrby('myrank', -10, 'Java')
# #print(newScore)
# #print(r.zscore('myrank', 'Java'))

# myrank = r.zrange('myrank', 0, 10, desc=True,withscores=True)

# print(str(myrank[0][0],'utf-8'))

# r.delete('myrank')
# print(r.ttl('myrank'))



#print(r.zrange("myrank",start=0,end=4,desc=True))
# import datetime
# import jwt
# playload = {
#                 'exp': int((datetime.datetime.now() + datetime.timedelta(seconds=20)).timestamp()),
#                 'data': {
#                     'uid':11
#                 }
# }
# encoded_jwt = jwt.encode(playload,'123',algorithm='HS256')
# print(encoded_jwt)
# de_code = jwt.decode('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzUzNDExOTAsImRhdGEiOnsidWlkIjoxMX19.iEjDQLIVmewe1wEs8YLW403x-alxpHEh9ymtXI2of0g','123',algorithms=['HS256'])
# print(de_code)


# import random
# import string

# code = string.ascii_letters + string.digits
# print(code)

# #获取随机生成的4位字符串
# def getCode():
#     return "".join(random.sample(code, 4))
# print(getCode())


# #获取4列4位激活码 以"—"串联起来
# def key(group):
#     return "—".join([getCode() for i in range(group)])
# print(key(4))



# import pymysql
# # 连接数据库
# conn = pymysql.connect(host='localhost',
#                        user='root',
#                        password='root',
#                        db='edu',
#                        charset='utf8')
# # 创建一个游标
# cursor = conn.cursor()
# # 插入数据
# # 数据直接写在sql后面
# sql = "insert into codes(code,discount) values ('%s',%s) " % (str(key(4)),'0.8')

# cursor.execute(sql)
# conn.commit()   # 提交，不然无法保存插入或者修改的数据(这个一定不要忘记加上)
# cursor.close()  # 关闭游标
# conn.close()

# import time
# def get_order_code():
#     order_no = str(time.strftime('%Y%m%d%H%M%S', time.localtime(time.time())))
#     return order_no

# print(get_order_code())


# import redis
# r = redis.Redis(host='localhost')

# r.set('127.0.0.1','127.0.0.1')
# r.expire('127.0.0.1',10)
# print(r.ttl('127.0.0.1'))
# print(r.get('127.0.0.1'))

# import hashlib

# md5 = hashlib.md5()

# sign_str = '123'
# sign_utf8 = str(sign_str).encode(encoding="utf-8")
# md5.update(sign_utf8)
# md5_server = md5.hexdigest()
# print(md5_server)
# import redis
# r = redis.Redis(host='localhost')

# r.sadd("set_name","bb")
# r.sadd("set_name","aa")

# r.expire('set_name',10)

# myset = r.smembers('set_name')
    
# print(myset)


# def merge_sort(arr):
#     sort(arr, 0, len(arr)-1)
 
# def sort(arr, low, high):
#     if low < high:
#         mid = (low + high) // 2
#         sort(arr, low, mid)
#         sort(arr, mid+1, high)
#         merge(arr, low, mid, high)
 
# def merge(arr, low, mid, high):
#     container = []    # 用于存储有序数字
#     i, j = low, mid+1
#     while i <= mid and j <= high:
#         if arr[i] <= arr[j]:
#             container.append(arr[i])
#             i += 1
#         else:
#             container.append(arr[j])
#             j += 1
#     if i <= mid:
#         container.extend(arr[i:mid+1])
#     elif j <= high:
#         container.extend(arr[j:high+1])
#     arr[low:high+1] = container
 
# # 做10次测试
# arr = [9,6,7,4,5,6]
# merge_sort(arr)
# print(arr)


# class Node(object):
#     def __init__(self, data, next = None):
#         self.data = data
#         self.next = next
 
# class Singlelinklist(object):
#     #创建空链表
#     def __init__(self):
#         self.head = None
#         self.tail = None
#     #测试是否为空
#     def isempty(self):
#         return(self.head is None)
#     #尾部增加元素
#     def append(self, data):
#         node = Node(data)
#         if self.head is None:
#             self.head = node
#             self.tail = node
#         else:
#             self.tail.next = node
#             self.tail = node
#     #删除一个元素
#     def remove(self, index):
#         if self.head is None:  #empty list
#             print('The linklist is empty!')
#             return
#         temp = self.head
#         temp_idx = 0
#         while temp_idx < index - 1:
#             temp = temp.next
#             if temp is None:
#                 print('List length less than index!')
#                 return
#             temp_idx += 1
#         if self.head is self.tail: #list only have one node
#             self.head = None
#             self.tail = None
#             return
#         if index == 0: #remove the first node
#             self.head = temp.next
#             return
#         if temp.next is None: #remove the last node
#             self.tail = temp
#         temp.next = temp.next.next
#     #遍历链表
#     def iter(self):
#         if not self.head:
#             return
#         temp = self.head
#         print(temp.data)
#         while temp.next:
#             temp = temp.next
#             print(temp.data)
#     #插入一个元素
#     def insert(self, index, value):
#         if self.head is None:
#             print('The list is empty!')
#             return
#         temp = self.head
#         temp_idx = 0
#         while temp_idx < index - 1:
#             temp = temp.next
#             temp_idx += 1
#         node = Node(value)
#         node.next = temp.next
#         temp.next = node
#         if node.next is None:
#             self.tail = node
#     #获取元素数
#     def size(self):
#         if self.head is None:
#             print('The list is empty!')
#             return
#         temp = self.head
#         count = 0
#         while temp is not None:
#             count += 1
#             temp = temp.next
#         return count
#     #查找一个元素
#     def search(self, item):
#         found = False
#         temp = self.head
#         while temp is not None and not found:
#             if temp.data == item:
#                 found = True
#             else:
#                 temp = temp.next
#         return found
 
# if __name__ == '__main__':
#     link = Singlelinklist()
#     link.append(3)
#     link.append(5)
#     link.append(6)
#     link.insert(1, 4)
#     link.iter()


# from qiniu import Auth
# q = Auth('q06bq54Ps5JLfZyP8Ax-qvByMBdu8AoIVJpMco2m','hfeEz-Lwzfg4ZYdNRW_TEw06S3HAjnIclu4WKB5O')
# token = q.upload_token('13123123123')
# print(token)

import hashlib
def make_password(mypass):
    #生成md5对象
    md5 = hashlib.md5()
    #定义加密对象
    sign_str = mypass
    #转码
    sign_utf8 = str(sign_str).encode(encoding="utf-8")
    #加密
    md5.update(sign_utf8)
    #生成密文
    md5_server = md5.hexdigest()
    return md5_server

print(make_password(123))