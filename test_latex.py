\usepackage{ctex}
%last edited: 16 July 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Probably the main one is this.
%"ModernCV" CV and Cover Letter
% LaTeX Template
% Version 1.1 (9/12/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Xavier Danaux (xdanaux@gmail.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
% Important note:
% This template requires the moderncv.cls and .sty files to be in the same 
% directory as this .tex file. These files provide the resume style and themes 
% used for structuring the document.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[UTF8]{moderncv} % Font sizes: 10, 11, or 12; paper sizes: a4paper, letterpaper, a5paper, legalpaper, executivepaper or landscape; font families: sans or roman

\moderncvstyle{classic} % CV theme - options include: 'casual' (default), 'classic', 'oldstyle' and 'banking'
\moderncvcolor{blue} % CV color - options include: 'blue' (default), 'orange', 'green', 'red', 'purple', 'grey' and 'black'

\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage[scale=0.85]{geometry} % Reduce document margins
\setlength{\hintscolumnwidth}{3cm} % Uncomment to change the width of the dates column
\setlength{\makecvtitlenamewidth}{10cm} % For the 'classic' style, uncomment to adjust the width of the space allocated to your name
\usepackage{fontawesome}
%----------------------------------------------------------------------------------------
%	NAME AND CONTACT INFORMATION SECTION
%----------------------------------------------------------------------------------------

\firstname{Liu} % Your first name
\familyname{Yue} % Your last name
% All information in this block is optional, comment out any lines you don't need
\title{Curriculum Vitae}
\address{342, Prestige Silver Crest}{Chandana, Bangalore 560103}
\mobile{+91 7693033044}
%\phone{(000) 111 1112}
%\fax{(000) 111 1113}
\email{usaxena95@gmail.com}
%\homepage{staff.org.edu/~jsmith}{staff.org.edu/$\sim$jsmith} % The first argument is the url for the clickable link, the second argument is the url displayed in the template - this allows special characters to be displayed such as the tilde in this example
%\extrainfo{additional information}
%\photo[70pt][0.4pt]{picture} % The first bracket is the picture height, the second is the thickness of the frame around the picture (0pt for no frame)
%\quote{"A witty and playful quotation" - John Smith}

%----------------------------------------------------------------------------------------

\begin{document}

\makecvtitle % Print the CV title

%----------------------------------------------------------------------------------------
%	EDUCATION SECTION
%----------------------------------------------------------------------------------------

\section{Education}

\cventry{August, 2013-- May, 2017}{Bachelor Of Technology In Computer Science And Engg}{IIT Indore}{}{\textit{CPI -- 9.5}}{}  % Arguments not required can be left empty
\cventry{April, 2011-- March, 2013}{Senior Secondary}{St. Francis' College}{Lucknow}{\textit{Percentage -- 96.2}}{\textbf{Top 1\%} in India}
\cventry{April, 2010-- March 2011}{Secondary}{St. Francis' College}{Lucknow}{\textit{Percentage -- 91.8}}{}

%\section{Masters Thesis}

%\cvitem{Title}{\emph{Money Is The Root Of All Evil -- Or Is It?}}
%\cvitem{Supervisors}{Professor James Smith \& Associate Professor Jane Smith}
%\cvitem{Description}{This thesis explored the idea that money has been the cause of untold anguish and suffering in the world. I found that it has, in fact, not.}

%----------------------------------------------------------------------------------------
%	WORK EXPERIENCE SECTION
%----------------------------------------------------------------------------------------

\section{Work Experience}

%\subsection{Vocational}


\cventry{June, 2018--Present}{Software Developer}{\textsc{Google}}{Munich}{}
{
	%%description
    %% Summer Internship 2016
}
\cventry{July, 2017--May, 2018}{Software Developer}{\textsc{Codenation}}{Bangalore}{}
{
	%%description
    %% Summer Internship 2016
}
\cventry{May, 2016--July, 2016}{Summer Intern}{\textsc{Amazon}}{Bangalore}{}
{
	%%description
    Summer Internship 2016
}
\cventry{May, 2015--July, 2015}{Summer Analyst}{\textsc{Morgan Stanley}}{Mumbai}{}
{
	%%description
    Summer Internship 2015
}
\cventry{2015--Present}{Algorithmic Problem Curator}{\textsc{HackerEarth}}{}{}{
%Developed spreadsheets for risk analysis on exotic derivatives on a wide array of commodities (ags, oils, precious and base metals), managed blotter and secondary trades on structured notes, liaised with Middle Office, Sales and Structuring for bookkeeping.
%\newline{}\newline{}
%Detailed achievements:
%\begin{itemize}
%\item Learned how to make amazing coffee
%\item Finally determined the reason for \textsc{PC LOAD LETTER}:
%\begin{itemize}
%\item Paper jam
%\item Software issues:
%\begin{itemize}
%\item Word not sending the correct data to printer
%\item Windows trying to print in letter format
%\end{itemize}
%\item Coffee spilled inside printer
%\end{itemize}
%\item Broke the office record for number of kitten pictures in cubicle
%\end{itemize}
}


%------------------------------------------------

%\subsection{Miscellaneous}

%\cventry{2008--2009}{Computer Repair Specialist}{Buy More}{Burbank}{}{Worked in the Nerd Herd and helped to solve computer problems by asking customers to turn their computers off and on again.}

%----------------------------------------------------------------------------------------
%	Achievements SECTION
%----------------------------------------------------------------------------------------

\section{Achievements}

\cvitem{ACM-ICPC World Finals 2017}{
\textbf{World Finals (Rapid City) 2017}. Rank \textbf{56} (out of 125 International Teams). Rank \textbf{2} among Indian teams. \href{https://icpc.baylor.edu/ICPCID/QTA31GR8HYJN}{\faExternalLink}
}

\cvitem{Codechef Snackdown 2016}{\textbf{Best Indian Team} (out of 13,000 teams with 17,238 global participants) 
\href{https://www.codechef.com/snackdown/2016/result/SNCKFL16}{\faExternalLink}
}

\cvitem{Think-A-Thon 2016}{Secured \textbf{1st} position in nationwide programming contest held by Hewlett-Packard Enterprise.
\href{https://he-s3.s3.amazonaws.com/media/uploads/cf95f36.jpg}{\faExternalLink}
}

\cvitem{Google APAC Test}
{
\textbf{APAC 2016, Round A: } \textbf{Rank 3} Globally out of 2280 participants.
\href{https://code.google.com/codejam/contest/4284486/scoreboard?c=4284486}{\faExternalLink}
\newline{}
%\textbf{APAC 2016, Round B: } \textbf{Rank 30} Globally out of 1190 participants.\newline{}
\textbf{APAC 2017, Round C: } \textbf{Rank 26} Globally out of 1600 participants.
\href{https://code.google.com/codejam/contest/6274486/scoreboard?c=6274486}{\faExternalLink}
\newline{}
%\textbf{APAC 2017, Round D: } \textbf{Rank 48} Globally out of 1528 participants.
}

\cvitem{CodeJam 2015}{\textbf{Google CodeJam 2015}. Qualified for Round 2 and \textbf{ranked 813}.(out of 24000+ participants) \href{https://code.google.com/codejam/contest/8234486/scoreboard\#sp=811}{\faExternalLink}
}

\cvitem{ACM-ICPC 2014}{\textbf{Asia-Kharagpur 2014}. Rank \textbf{8th} in Regionals held at IIT Kharagpur(out of 76 Indian Teams)\newline
\textbf{Asia-Amritapuri 2014}. Rank \textbf{6th} in Regionals held at Amrita University(out of 270 Indian Teams)
}

\cvitem{ACM-ICPC 2016}{
\textbf{Asia-Amritapuri 2016}. Rank \textbf{4th} in Regionals held at Amrita University(out of 450 Indian Teams)
\newline
\textbf{Asia-India Finals (Gwalior) 2016}. Rank \textbf{3rd} in India-Finals held at IIITM, Gwalior (out of Top 100 Indian Teams qualified from other Indian Regionals)
}

%----------------------------------------------------------------------------------------
%	Programming Profiles
%^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

\newpage
\section{Online Programming Profiles}
\cvitem{HackerRank}{
Handle: \textbf{usaxena95} 
\href{https://www.hackerrank.com/usaxena95?hr_r=1}{\faExternalLink}
\newline
Current Rating: 2567
(\textbf{\href{https://www.hackerrank.com/leaderboard?country=India&level=1&page=1}{Rank 9 in India \faExternalLink}})
}

\cvitem{Codechef}{
Handle: \textbf{usaxena95} \href{https://www.codechef.com/users/usaxena95}{\faExternalLink} \newline
Current Rating: 2505 (7\faStar) \newline{}
India Rank: \textbf{6},
Global Rank: \textbf{25}\newline{}
\textbf{Six Top10} and \textbf{Two Top3} finish in Codechef Contests in India.
}

\cvitem{Codeforces}{
Handle: \textbf{usaxena95} 
\href{http://codeforces.com/profile/usaxena95}{\faExternalLink}
\newline{}
Current Rating: 2036(\textbf{Candidate Master})
}

%----------------------------------------------------------------------------------------
%	Projects SECTION
%^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
% Remember to remove if it looks odd.
%----------------------------------------------------------------------------------------
\section{Projects}
\cvitem{July - October 2017(Codenation)}{\textbf{Cost Allocation \& Calculation Engine:} 
Involved executing hundreds of process equations on each row of database. Converted these row wise operations into equivalent vectorized operations for faster calculation using \textit{TensorFlow/PyGDF/Spark DF/Pandas DF}.\newline
Used \textit{ANTLR} to compile all process equations into \textit{python-pandas} equivalent vectorized operations. 
Stored the results in a \textit{columnar database} for faster slice-dice operations involved in report generation and huge data compression ratio.
}

\cvitem{Jan - April 2016}{\textbf{Real Time Physics Simulation:} Implemented Rigid Body Dynamics and Gravitational forces in 3D using OpenGL and GLUT in C++ to simulate interactive multiple rigid body collisions and gravitational orbits.}

\cvitem{Jan - April 2016}{\textbf{Sentence and Sentiment Classifier}: Implemented Sequential Minimal Optimization(SMO) Algorithm for  classification of sentences into Declarative, Imperative, Interrogative category and sentiment analysis. Compared results with Naive Bayes classifier.}

\cvitem{Summer 2015}{Developed plugin for monitoring user stats and activities for various service gateways(in Morgan Stanley)}

\cvitem{Summer 2015}{Developed a daily email generator that processed various raw data sets and emails and stores the processed data in hierarchical database schema(in Morgan Stanley) }

\cvitem{Jan - April 2015}{\textbf{PDF Converter and Search : } Made a converter that converted tabular data in pdf into different categorized csv files. Performed various types of range search queries on the created CSV files}

\cvitem{July - November 2013}{\textbf{Mario Game 2D:} Made a single staged 2D Mario game with basic key operations using Simple DirectMedia Layer in C++}
%----------------------------------------------------------------------------------------
%	Position of Responsibility SECTION
%----------------------------------------------------------------------------------------

\section{Position of Responsibility}

\cvitem{2014-2016}{Member of Programming Club, IIT INDORE}
  \cvitem{2015-Present}{Member of Tech Team of FLUXUS(Annual Cultural Fest in IIT Indore)}


%----------------------------------------------------------------------------------------
%	INTERESTS SECTION
%----------------------------------------------------------------------------------------

\section{Interests}

\renewcommand{\listitemsymbol}{-~} % Changes the symbol used for lists

%\cvlistdoubleitem{Piano}{Chess}
%\cvlistdoubleitem{Cooking}{Dancing}
\cvlistitem{Algorithms and Data Structures}
%----------------------------------------------------------------------------------------
%	TECHNICAL SKILLS SECTION
%----------------------------------------------------------------------------------------

\section{Technical skills}

%\cvitem{OS}{Linux, Unix,Windows}
\cvitem{Languages}{C++(STL), C, Python, Java}
\cvitem{Framework}{Spring, Django}
%----------------------------------------------------------------------------------------
%	COVER LETTER
%----------------------------------------------------------------------------------------

% To remove the cover letter, comment out this entire block

%\clearpage

%\recipient{HR Departmnet}{Corporation\\123 Pleasant Lane\\12345 City, State} % Letter recipient
%\date{\today} % Letter date
%\opening{Dear Sir or Madam,} % Opening greeting
%\closing{Sincerely yours,} % Closing phrase
%\enclosure[Attached]{curriculum vit\ae{}} % List of enclosed documents

%\makelettertitle % Print letter title

%\lipsum[1-3] % Dummy text

%\makeletterclosing % Print letter signature

%----------------------------------------------------------------------------------------

\end{document}