import smtplib
from email.mime.text import MIMEText
from email.utils import formataddr
import time


#jwt秘钥
jwt_code = 'mycode'

my_sender='3611792766@qq.com'    # 发件人邮箱账号
my_pass = 'ihzsfjvctqdmciff'              # 发件人smtp秘钥(当时申请smtp给的口令)

def mail(subject,text,my_user):
    ret=True
    try:
        msg=MIMEText(text,'plain','utf-8')
        msg['From']=formataddr(["发件人昵称",my_sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
        msg['To']=formataddr(["收件人昵称",my_user])              # 括号里的对应收件人邮箱昵称、收件人邮箱账号
        msg['Subject']=subject              # 邮件的主题，也可以说是标题

        server=smtplib.SMTP_SSL("smtp.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是465
        server.login(my_sender, my_pass)  # smtp秘钥
        server.sendmail(my_sender,[my_user,],msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件
        server.quit()# 关闭连接
    except Exception:# 如果 try 中的语句没有执行，则会执行下面的 ret=False
        ret=False
    return ret


#递归树

def xTree(datas):
    lists=[]
    tree={}
    parent_id=''
    for i in datas:
        item=i
        tree[item['id']]=item
    root=None
    for i in datas:
        obj=i
        if not obj['pid']:
            root=tree[obj['id']]
            lists.append(root)
        else:
            parent_id=obj['pid']
            if 'childlist' not in tree[parent_id]:   
                tree[parent_id]['childlist']=[]
            tree[parent_id]['childlist'].append(tree[obj['id']])
    return lists


#根据日期生成唯一订单号
def get_order_code():
    order_no = str(time.strftime('%Y%m%d%H%M%S', time.localtime(time.time())))
    return order_no