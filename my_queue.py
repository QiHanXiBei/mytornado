class MyQueue:
    def __init__(self):
        self.s = []
    def push(self, x: tuple) -> None:
        self.s.append(x)
    def pop(self) -> tuple:
        self.s = sorted(self.s, key=lambda k: k[0],reverse=True)
        return self.s.pop(0)
    def empty(self) -> bool:
        return not bool(self.s)

myq = MyQueue()
myq.push((0,1))
myq.push((0,3))
myq.push((1,2))
myq.push((0,1))
myq.push((0,3))
myq.push((1,2))

print(myq.pop())


from concurrent.futures import ThreadPoolExecutor