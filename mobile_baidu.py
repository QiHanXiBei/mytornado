import requests
from bs4 import BeautifulSoup
from lxml import etree
import re

headersParameters = {  # 发送HTTP请求时的HEAD信息，用于伪装为浏览器
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'Accept-Encoding': 'gzip, deflate',
    'User-Agent': 'Mozilla/6.1 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}

keyword = "百瑞赢"

url = u'https://m.baidu.com/s?word='+keyword+'&pn=10'

html = requests.get(url ,timeout=200, headers=headersParameters).text

soup = BeautifulSoup(html, 'lxml')
div3 = soup.find_all(name='div',attrs={"class":"c-touchable-feedback"})

for h3 in div3:
    try:
        table=h3.find('span').get_text()
        print(table)
        href = h3.find('a').get('href')
        print(href)
        baidu_url = requests.get(url=href,headers=headersParameters,allow_redirects=True).text
        searchObj = re.findall(r'window\.location\.replace\(\"(.*)\"\)',baidu_url,re.I)
        if len(searchObj) > 0:
            source_url = searchObj[0]
        print(source_url)
        
    except Exception as e:
        print(str(e))

exit(-1)


           

