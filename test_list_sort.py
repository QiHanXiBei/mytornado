import functools
current_users = [6, 2, 2, 1]
new_users = [3, 2, 1, 2]

import sys

class Person:
    pass

p1 = Person() # 1

p2 = p1

print(id(p1))
print(id(p2))

print(sys.getsizeof(p2))



class Person:
    pass

class Dog:
    pass

p = Person() 
d = Dog()   

p.pet = d 
d.master = p

print(d.master)



